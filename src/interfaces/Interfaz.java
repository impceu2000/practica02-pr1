package interfaces;

import dominio.CatalogoDeRopa;
import dominio.Prenda;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
        private static CatalogoDeRopa catalogoDeRopa = new CatalogoDeRopa();

        private static void mostrarAyuda(){
                System.out.println("Las instrucciones posibles son las siguientes:");
                System.out.println("    1. Mostrar prendas: java -jar catalogoDeRopa.jar show");
                System.out.println("    2. Mostrar esta ayuda: java -jar catalogoDeRopa.jar help");
                System.out.println("    3. Añadir prenda: java -jar catalogoDeRopa.jar add <nombre> <marca> <talla> <stock>, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar add Cortavientos ASSC XL 20");
	}
        

        public static void ejecutar(String[] args){
                try
                {
                        if (args[0].equalsIgnoreCase("add"))
                                catalogoDeRopa.annadirPrenda(new Prenda(args[1], args[2], args[3], args[4]));
                        else if (args[0].equalsIgnoreCase("show")){
                                catalogoDeRopa.mostrarPrendas();
		}
                        else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
                        else mostrarAyuda();
                }catch(ArrayIndexOutOfBoundsException ex){
                        mostrarAyuda();
                }
        }
}
