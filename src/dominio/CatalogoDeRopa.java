package dominio;


import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;


public class CatalogoDeRopa{
        private ArrayList<Prenda> listaPrendas = new ArrayList<>();
        private static String nombreFichero = "prendas.txt";

        public CatalogoDeRopa(){
                cargarDesdeFichero();
        }

        public void annadirPrenda(Prenda prenda){
                listaPrendas.add(prenda);
                volcarAFichero();
        }

        public void mostrarPrendas()
        {
                for(Prenda prenda : listaPrendas)
                        System.out.println(prenda);
        }

        private void volcarAFichero(){
                try{
                        FileWriter fw = new FileWriter(nombreFichero);
                        fw.write(this.toString());
                        fw.close();
                }catch(IOException ex){
                        System.err.println("Error al intentar escribir en fichero");
                }
        }

        private void cargarDesdeFichero(){
                try{
                        File fichero = new File(nombreFichero);
                        if (fichero.createNewFile()) {
                                System.out.println("Acaba de crearse un nuevo fichero");
			} else {
                                Scanner sc = new Scanner(fichero);
                                while(sc.hasNext()){
                                        listaPrendas.add(new Prenda(sc.next(), sc.next(), sc.next(), sc.next()));
                                }
                        }
                }catch(IOException ex){
                }
        }

        public String toString() {
                StringBuilder sb = new StringBuilder();
                for(Prenda prenda : listaPrendas) sb.append(prenda + "\n");
                return sb.toString();
        }
}




