# Notas para los usuarios

## Compilación del programa

Se ejecuta la siguiente instrucción:

~~~~
make jar
~~~~

## Uso del catalogo de ropa:

Permite ejecutar las instrucciones que se muestran a continuación: 
     
1. Mostrar todas las prendas:

~~~~
 java -jar catalogoDeRopa.jar show
~~~~

2. Mostrar ayuda: 

~~~~
java -jar catalogoDeRopa.jar help
~~~~

3. Añadir prenda:

~~~~
java -jar catalogoDeRopa.jar add <nombre> <marca> <talla> <stock>
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar add Zapatos Scalpers 39 55
~~~~

# Notas para los desarrolladores

## Generación de Javadoc

Se ejecuta la siguiete instrucción:

~~~~
make javadoc
~~~~ 

## Inspección de Javadoc

Suponiendo que tiene instalado `firefox`, se ejecuta:

~~~~
firefox htlm/index.html
~~~~

## Sobre el fichero _makefile_

Se han utilizado sentencias específicas de Linux, por tanto, sólo
ejecuta en este sistema operativo.

